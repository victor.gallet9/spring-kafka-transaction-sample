package spring.transaction.sample

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
open class IntegrationTest {

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    @Value("\${incoming.topic.db.first:$INCOMING_TOPIC_DB_FIRST}")
    lateinit var topicIncomingDBFirst: String

    @Value("\${incoming.reactive.topic:$INCOMING_REACTIVE_TOPIC}")
    lateinit var topicIncomingReactive: String

    @Value("\${outgoing.reactive.topic:${OUTGOING_REACTIVE_TOPIC}")
    lateinit var topicOutgoingReactive: String

    @Value("\${outgoing.topic.db.first:$OUTGOING_TOPIC_DB_FIRST}")
    lateinit var topicOutgoingDBFirst: String


    @Value("\${incoming.topic.kafka.first:$INCOMING_TOPIC_KAFKA_FIRST}")
    lateinit var topicIncomingKafkaFirst: String


    @Value("\${outgoing.topic.kafka.first:$OUTGOING_TOPIC_KAFKA_FIRST}")
    lateinit var topicOutgoingKafkaFirst: String


    @BeforeEach
    internal open fun setUp() {
        MESSAGES.clear()
        jdbcTemplate.execute("DELETE FROM mytable")
        System.setProperty(FAILING_CONSUMER, "false")
        System.setProperty(FAILING_KAFKA_PRODUCER, "false")
    }

    @AfterEach
    internal open fun tearDown() {

    }
}
