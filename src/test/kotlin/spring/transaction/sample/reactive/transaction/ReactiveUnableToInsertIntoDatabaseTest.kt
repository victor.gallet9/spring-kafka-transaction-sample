package spring.transaction.sample.reactive.transaction

import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility
import org.awaitility.Durations
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import spring.transaction.sample.ApplicationContainerInitializer
import spring.transaction.sample.IntegrationTest
import spring.transaction.sample.MESSAGES

@SpringBootTest(
    properties = [
        "incoming.reactive.topic=reactive.transaction.failing.database.incoming",
        "outgoing.reactive.topic=reactive.transaction.failing.database.outgoing"]
)
@ContextConfiguration(
    initializers = [ApplicationContainerInitializer::class]
)
@DirtiesContext
class ReactiveUnableToInsertIntoDatabaseTest : IntegrationTest() {

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @Test
    fun should_not_send_message_into_kafka_nor_insert_into_database_with_database_error() {
        // GIVEN
        // A message is produced into incoming topic
        val expected_data = "unable_to_insert_into_database_because_the_message_is_too_long"
        kafkaTemplate.executeInTransaction { t: KafkaOperations<String?, String?> ->
            t.send(topicIncomingReactive, expected_data)
        }

        // THEN
        // The database is not updated
        // No Kafka Message is sent
        Awaitility.await()
            .timeout(Durations.TEN_SECONDS)
            .pollDelay(Durations.FIVE_SECONDS)
            .untilAsserted {
                val data = jdbcTemplate.queryForList("SELECT data from mytable", String::class.java)
                println("Data List: $data")
                assertThat(data).`as`("Database is empty").isEmpty()

                assertThat(MESSAGES).`as`("kafka messages not send").isEmpty()
            }
    }
}