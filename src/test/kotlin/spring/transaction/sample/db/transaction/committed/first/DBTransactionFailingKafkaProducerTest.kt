package spring.transaction.sample.db.transaction.committed.first

import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility
import org.awaitility.Durations
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import spring.transaction.sample.ApplicationContainerInitializer
import spring.transaction.sample.ApplicationContainerInitializer.Companion.proxy
import spring.transaction.sample.FAILING_KAFKA_PRODUCER
import spring.transaction.sample.IntegrationTest
import spring.transaction.sample.MESSAGES
import java.time.Duration

@SpringBootTest(
    properties = [
        "incoming.topic.db.first=db.transaction.failing.producer.test.incoming",
        "outgoing.topic.db.first=db.transaction.failing.producer.test.outgoing"]
)
@ContextConfiguration(
    initializers = [ApplicationContainerInitializer::class]
)
@DirtiesContext
class DBTransactionFailingKafkaProducerTest : IntegrationTest() {

    @BeforeEach
    override fun setUp() {
        super.setUp()
    }

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @AfterEach
    override fun tearDown() {
//        proxy.toxics().get("CUT_CONNECTION_DOWNSTREAM").remove()
//        proxy.toxics().get("CUT_CONNECTION_UPSTREAM").remove()
    }

    @Test
//    @Disabled
    fun should_not_send_message_into_kafka_nor_insert_into_database_witth_failing_producer() {
        // GIVEN
        // A message is produced into incoming topic
        val expected_data = "failing_producer"
        kafkaTemplate.executeInTransaction { t: KafkaOperations<String?, String?> ->
            t.send(topicIncomingDBFirst, expected_data)
            .whenComplete { meta, exception ->
                System.setProperty(FAILING_KAFKA_PRODUCER, "true")
            }
        }

        // THEN
        // The database is not updated
        // No Kafka Message is sent
        Awaitility.await()
            .timeout(Durations.TEN_SECONDS)
            .pollDelay(Durations.FIVE_SECONDS)
            .untilAsserted {
                val data = jdbcTemplate.queryForList("SELECT data from mytable", String::class.java)
                println("Data List: $data")
                assertThat(data).`as`("Database is empty").isEmpty()

                assertThat(MESSAGES).`as`("kafka messages not send").isEmpty()
            }
    }
}