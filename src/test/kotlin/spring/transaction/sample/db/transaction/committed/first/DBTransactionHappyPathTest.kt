package spring.transaction.sample.db.transaction.committed.first

import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility
import org.awaitility.Durations
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import spring.transaction.sample.ApplicationContainerInitializer
import spring.transaction.sample.INCOMING_TOPIC_DB_FIRST
import spring.transaction.sample.IntegrationTest
import spring.transaction.sample.MESSAGES


@SpringBootTest(
    properties = [
        "incoming.topic.db.first=db.transaction.happy.incoming",
        "outgoing.topic.db.first=db.transaction.happy.outgoing"]
)
@ContextConfiguration(
    initializers = [ApplicationContainerInitializer::class]
)
@DirtiesContext
class DBTransactionHappyPathTest : IntegrationTest() {

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @Test
    fun should_consume_and_write_to_database() {
        // GIVEN
        // A message is produced into incoming topic
        val expected_data = "happy_path"
        kafkaTemplate.executeInTransaction { t: KafkaOperations<String?, String?> ->
            t.send(topicIncomingDBFirst, expected_data)
        }

        // THEN
        // The database is updated
        Awaitility.await()
            .atMost(Durations.FIVE_SECONDS)
            .untilAsserted {
                val data = jdbcTemplate.queryForList("SELECT data from mytable", String::class.java)
                assertThat(data).hasSize(1).contains(expected_data)

                assertThat(MESSAGES).contains(expected_data)
            }
    }
}