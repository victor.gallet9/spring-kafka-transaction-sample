package spring.transaction.sample

import org.apache.kafka.clients.admin.NewTopic
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.KafkaOperations
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.listener.AfterRollbackProcessor
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer
import org.springframework.kafka.listener.DefaultAfterRollbackProcessor
import org.springframework.util.backoff.FixedBackOff
import javax.sql.DataSource


fun main(args: Array<String>) {
    val application = SpringApplication(Application::class.java)

    application.addInitializers(ApplicationContainerInitializer())

    application.run(*args)
}

const val FAILING_CONSUMER = "FAILING_CONSUMER"

const val FAILING_KAFKA_PRODUCER = "FAILING_KAFKA_PRODUCER"

const val INCOMING_TOPIC_DB_FIRST = "incoming_topic_db_first"

const val OUTGOING_TOPIC_DB_FIRST = "outgoing_topic_db_first"

const val INCOMING_TOPIC_KAFKA_FIRST = "incoming_topic_kafka_first"

const val OUTGOING_TOPIC_KAFKA_FIRST = "outgoing_topic_kafka_first"

const val INCOMING_REACTIVE_TOPIC = "incoming_reactive_topic"

const val OUTGOING_REACTIVE_TOPIC = "outgoing_reactive_topic"

val MESSAGES = mutableListOf<String>()

@SpringBootApplication
class Application {

    @Bean
    fun rollbackProcessor(kafkaTemplate: KafkaTemplate<String, String>): AfterRollbackProcessor<Any?, Any?> {
        val deadLetterPublishingRecoverer = DeadLetterPublishingRecoverer(kafkaTemplate)
        return DefaultAfterRollbackProcessor<Any?, Any?>(deadLetterPublishingRecoverer, FixedBackOff(0L, 0L))
    }

    @Bean
    @Profile(value = ["!test"])
    fun runner(
        template: KafkaTemplate<String?, String?>,
        @Value("\${incoming.topic.db.first:$INCOMING_TOPIC_DB_FIRST}") incomingDBTopic: String,
        @Value("\${incoming.topic.kafka.first:${INCOMING_TOPIC_KAFKA_FIRST}") incomingKafkaTopic: String): ApplicationRunner? {
        return ApplicationRunner {
            template.executeInTransaction { t: KafkaOperations<String?, String?> ->
                t.send(
                    incomingDBTopic,
                    "test"
                )
                t.send(incomingKafkaTopic, "test")
            }
        }
    }


    @Bean
    fun dstm(dataSource: DataSource): DataSourceTransactionManager? {
        return DataSourceTransactionManager(dataSource)
    }

    @Bean
    fun topic1(@Value("\${incoming.topic.db.first:$INCOMING_TOPIC_DB_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }

    @Bean
    fun dltTopic1(@Value("\${incoming.topic.db.first:$INCOMING_TOPIC_DB_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name("$topic.DLT").build()
    }

    @Bean
    fun topic2(@Value("\${outgoing.topic.db.first:$OUTGOING_TOPIC_DB_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }

    @Bean
    fun topic3(@Value("\${incoming.topic.kafka.first:$INCOMING_TOPIC_KAFKA_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }

    @Bean
    fun dltTopic3(@Value("\${incoming.topic.kafka.first:$INCOMING_TOPIC_KAFKA_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name("$topic.DLT").build()
    }

    @Bean
    fun topic4(@Value("\${outgoing.topic.kafka.first:$OUTGOING_TOPIC_KAFKA_FIRST}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }

    @Bean
    fun topic5(@Value("\${incoming.reactive.topic:$INCOMING_REACTIVE_TOPIC}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }

    @Bean
    fun topic6(@Value("\${outgoing.reactive.topic:$OUTGOING_REACTIVE_TOPIC}") topic: String): NewTopic? {
        return TopicBuilder.name(topic).build()
    }
}