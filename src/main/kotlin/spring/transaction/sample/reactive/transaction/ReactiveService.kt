package spring.transaction.sample.reactive.transaction

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import spring.transaction.sample.FAILING_CONSUMER
import spring.transaction.sample.OUTGOING_REACTIVE_TOPIC

@Service
class ReactiveService(
    val jdbcTemplate: JdbcTemplate,
    val kafkaTemplate: KafkaTemplate<String, String>
) {

    @Value("\${outgoing.reactive.topic:$OUTGOING_REACTIVE_TOPIC}")
    lateinit var outgoingReactiveTopic: String

    @Transactional("dstm")
    fun consume(record: ConsumerRecord<String, String>) {
        println("Consuming incoming message ${record.value()} - topic ${record.topic()} - offset ${record.offset()}")
        println("Inserting into DB")
        jdbcTemplate.execute("insert into mytable (data) values ('${record.value()}')")

        if (System.getProperty(FAILING_CONSUMER, "false").toBoolean()) {
            throw RuntimeException("failing consumer")
        }
    }
}