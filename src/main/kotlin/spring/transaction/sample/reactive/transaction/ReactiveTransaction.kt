package spring.transaction.sample.reactive.transaction

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.TransactionManager
import reactor.util.retry.Retry
import spring.transaction.sample.MESSAGES
import spring.transaction.sample.OUTGOING_REACTIVE_TOPIC
import java.time.Duration

@Component
class ReactiveTransaction(
    val kafkaIncomingConsumerTemplate: ReactiveKafkaConsumerTemplate<String, String>,
    val kafkaOutgoingConsumerTemplate: ReactiveKafkaConsumerTemplate<String, String>,
    val senderOptions: SenderOptions<String, String>,
    val reactiveService: ReactiveService
) {

    @Value("\${outgoing.reactive.topic:$OUTGOING_REACTIVE_TOPIC}")
    lateinit var outgoingReactiveTopic: String

    @EventListener(classes = [ApplicationReadyEvent::class])
    fun incomingEventListener() {
        val kafkaSender = KafkaSender.create(senderOptions)
        val transactionManager = kafkaSender.transactionManager()
        kafkaIncomingConsumerTemplate
            .receiveExactlyOnce(transactionManager)
            .doOnError { println("Error occurred when trying to process incoming event") }
            .retryWhen(Retry.fixedDelay(Long.MAX_VALUE, Duration.ofSeconds(5)))
            .doOnSubscribe { println("Processing incoming event") }
            .concatMap {
                f ->
                process(kafkaSender, f, transactionManager)
            }
            .onErrorResume {
                println("Error $it")
                Flux.empty()
            }
            .subscribe()
    }

    @Transactional("dstm")
    fun process(
        kafkaSender: KafkaSender<String, String>,
        f: Flux<ConsumerRecord<String, String>>,
        transactionManager: TransactionManager
    ) = kafkaSender.send(f.map { record -> transform(record) })
        .concatWith(transactionManager.commit())

    private fun transform(record: ConsumerRecord<String, String>): SenderRecord<String, String, String>? {
        reactiveService.consume(record);
        return SenderRecord.create(
            ProducerRecord(
                outgoingReactiveTopic,
                record.value()
            ),
            record.value()
        )
    }


    @EventListener(classes = [ApplicationReadyEvent::class])
    fun outgoingEventListener() {
        kafkaOutgoingConsumerTemplate.receive()
            .doOnError { println("Error occurred when trying to process outgoing event") }
            .retryWhen(Retry.fixedDelay(Long.MAX_VALUE, Duration.ofSeconds(5)))
            .doOnSubscribe { println("Processing outgoing event") }
            .concatMap { record ->
                println("Consuming outgoing message ${record.value()} - topic ${record.topic()} - offset ${record.offset()}")
                MESSAGES.add(record.value())
                Mono.just(MESSAGES);
            }
            .subscribe()
    }
}