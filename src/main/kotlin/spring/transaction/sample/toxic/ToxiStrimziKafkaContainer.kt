package spring.transaction.sample.toxic

import io.strimzi.test.container.StrimziKafkaContainer

class ToxiStrimziKafkaContainer(private val toxiHost: String, val toxiPort: Int) : StrimziKafkaContainer() {

    override fun getBootstrapServers(): String {
        return String.format("PLAINTEXT://%s:%s", toxiHost, toxiPort)
    }
}