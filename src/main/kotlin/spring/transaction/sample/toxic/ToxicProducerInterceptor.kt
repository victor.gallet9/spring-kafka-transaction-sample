package spring.transaction.sample.toxic

import org.apache.kafka.clients.producer.ProducerInterceptor
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import spring.transaction.sample.FAILING_KAFKA_PRODUCER
import java.lang.Exception

class ToxicProducerInterceptor : ProducerInterceptor<String, String> {
    override fun configure(configs: MutableMap<String, *>?) {
    }

    override fun close() {
    }

    override fun onAcknowledgement(metadata: RecordMetadata?, exception: Exception?) {
    }

    override fun onSend(record: ProducerRecord<String, String>?): ProducerRecord<String, String> {
        if (System.getProperty(FAILING_KAFKA_PRODUCER, "false").toBoolean() && !record!!.topic().endsWith("DLT")) {
            return ProducerRecord(record!!.topic(), CharArray(10000) { ('a'..'z').random() }.concatToString())
        }
        return record!!
    }
}