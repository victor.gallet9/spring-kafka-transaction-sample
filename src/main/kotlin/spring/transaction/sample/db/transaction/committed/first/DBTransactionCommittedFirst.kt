package spring.transaction.sample.db.transaction.committed.first

import eu.rekawek.toxiproxy.model.ToxicDirection
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import spring.transaction.sample.ApplicationContainerInitializer
import spring.transaction.sample.FAILING_CONSUMER
import spring.transaction.sample.FAILING_KAFKA_PRODUCER
import spring.transaction.sample.INCOMING_TOPIC_DB_FIRST
import spring.transaction.sample.MESSAGES
import spring.transaction.sample.OUTGOING_TOPIC_DB_FIRST

@Component
class DBTransactionCommittedFirst(val kafkaTemplate: KafkaTemplate<String, String>, val jdbcTemplate: JdbcTemplate) {

    @Value("\${outgoing.topic.db.first:$OUTGOING_TOPIC_DB_FIRST}")
    lateinit var topicOutgoingDBFirst: String

    @KafkaListener(id = "group1", topics = ["\${incoming.topic.db.first:$INCOMING_TOPIC_DB_FIRST}"])
    @Transactional("dstm")
    fun listen1(record: ConsumerRecord<String, String>) {
        println("Consuming message ${record.value()} - topic ${record.topic()} - offset ${record.offset()}")

//        if (System.getProperty(FAILING_KAFKA_PRODUCER, "false").toBoolean()) {
//            val toxics = ApplicationContainerInitializer.proxy.toxics()
//            if (toxics.all.isEmpty()) {
//                toxics.bandwidth("CUT_CONNECTION_DOWNSTREAM", ToxicDirection.DOWNSTREAM, 0)
//                toxics.bandwidth("CUT_CONNECTION_UPSTREAM", ToxicDirection.UPSTREAM, 0)
//            }
//        }

        println("Inserting into DB")
        jdbcTemplate.execute("insert into mytable (data) values ('${record.value()}')")

        println("Sending to KAFKA")
        var send = kafkaTemplate.send(topicOutgoingDBFirst, record.value())
        send.whenComplete { meta, exception ->
            if (exception != null) {
                println("UNABLE TO SEND message ${record.value()}")
            }
        }

        if (System.getProperty(FAILING_CONSUMER, "false").toBoolean()) {
            throw RuntimeException("failing consumer")
        }
    }

    @KafkaListener(id = "group2", topics = ["\${outgoing.topic.db.first:$OUTGOING_TOPIC_DB_FIRST}"])
    fun listen2(record: ConsumerRecord<String, String>) {
        println("Consuming message ${record.value()} - topic ${record.topic()} - offset ${record.offset()}")
        MESSAGES.add(record.value());
    }
}