package spring.transaction.sample

import com.github.dockerjava.api.command.CreateContainerCmd
import com.github.dockerjava.api.model.ExposedPort
import com.github.dockerjava.api.model.PortBinding
import com.github.dockerjava.api.model.Ports
import eu.rekawek.toxiproxy.Proxy
import eu.rekawek.toxiproxy.ToxiproxyClient
import io.strimzi.test.container.StrimziKafkaContainer
import org.junit.Rule
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.core.env.MapPropertySource
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.ToxiproxyContainer
import org.testcontainers.lifecycle.Startables
import org.testcontainers.shaded.org.awaitility.Awaitility.await
import org.testcontainers.utility.DockerImageName
import spring.transaction.sample.toxic.ToxiStrimziKafkaContainer
import java.util.Collections
import java.util.function.Consumer


class ApplicationContainerInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

    companion object {

        private const val kafkaImage = "confluentinc/cp-kafka:7.2.1"

        private const val postgresqlImage = "library/postgres:12-alpine"

        private const val toxiproxyImage = "ghcr.io/shopify/toxiproxy:2.4.0"

        @Rule
        private var network: Network = Network.newNetwork()

        var proxy: Proxy

        val toxiproxyContainer = ToxiproxyContainer(
            DockerImageName
                .parse(toxiproxyImage)
                .asCompatibleSubstituteFor("shopify/toxiproxy")
        )
        .withNetwork(network)
        .withNetworkAliases("toxiproxy")

        var kafkaContainer: StrimziKafkaContainer

        private val postgreSQLContainer = PostgreSQLContainer(
            DockerImageName
                .parse(postgresqlImage)
                .asCompatibleSubstituteFor("postgres")
        )
        .withExposedPorts(5432)
        .withNetwork(network)
        .withNetworkAliases("bdd")
        .withInitScript("init.sql");

        init {
            toxiproxyContainer.start()
            await().until(toxiproxyContainer::isRunning)

            val toxiproxyClient = ToxiproxyClient(toxiproxyContainer.host, toxiproxyContainer.controlPort)
            proxy = toxiproxyClient.createProxy("kafka:9092", "0.0.0.0:8666", "kafka:9092")

            kafkaContainer = ToxiStrimziKafkaContainer(toxiHost = toxiproxyContainer.host, toxiPort = toxiproxyContainer.getMappedPort(8666))
                .withExposedPorts(9092)
                .withNetwork(toxiproxyContainer.network)
                .withNetworkAliases("kafka")

            Startables.deepStart(kafkaContainer, postgreSQLContainer).join()
        }
    }

    override fun initialize(context: ConfigurableApplicationContext) {
        val properties = mapOf(
            "spring.datasource.url" to postgreSQLContainer.jdbcUrl,
            "spring.datasource.username" to postgreSQLContainer.username,
            "spring.datasource.password" to postgreSQLContainer.password,

            "spring.kafka.bootstrap-servers" to "${toxiproxyContainer.host}:${toxiproxyContainer.getMappedPort(8666)}",
        );

        val env: ConfigurableEnvironment = context.environment
        env.propertySources.addFirst(MapPropertySource("testcontainers", properties))
    }
}