package spring.transaction.sample

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import java.util.*

@Configuration
class ReactiveConfiguration {

    @Bean
    fun senderOptions(kafkaProperties: KafkaProperties): SenderOptions<String, String> {
        return SenderOptions
            .create<String, String>(kafkaProperties.buildProducerProperties())
            .producerProperty(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "SampleTxn")
    }

    @Bean
    fun sender(options: SenderOptions<String, String>): KafkaSender<String, String>? {
        return KafkaSender.create(options);
    }

    @Bean
    fun reactiveTransactionalKafkaProducerTemplate(senderOptions: SenderOptions<String, String>): ReactiveKafkaProducerTemplate<String, String> {
        return ReactiveKafkaProducerTemplate(senderOptions)
    }


    @Bean
    fun kafkaIncomingConsumerTemplate(kafkaProperties: KafkaProperties, @Value("\${incoming.reactive.topic:$INCOMING_REACTIVE_TOPIC}") topic: String) =
        ReceiverOptions
            .create<String, String>(kafkaProperties.buildConsumerProperties())
            .consumerProperty(ConsumerConfig.GROUP_ID_CONFIG, "reactiveGroup1")
            .subscription(Collections.singletonList(topic))
            .let { ReactiveKafkaConsumerTemplate(it) }

    @Bean
    fun kafkaOutgoingConsumerTemplate(kafkaProperties: KafkaProperties, @Value("\${outgoing.reactive.topic:$OUTGOING_REACTIVE_TOPIC}") topic: String) =
        ReceiverOptions
            .create<String, String>(kafkaProperties.buildConsumerProperties())
            .consumerProperty(ConsumerConfig.GROUP_ID_CONFIG, "reactiveGroup2")
            .subscription(Collections.singletonList(topic))
            .let { ReactiveKafkaConsumerTemplate(it) }
}