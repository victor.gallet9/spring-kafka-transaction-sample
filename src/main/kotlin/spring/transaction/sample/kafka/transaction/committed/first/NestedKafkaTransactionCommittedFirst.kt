package spring.transaction.sample.kafka.transaction.committed.first

import eu.rekawek.toxiproxy.model.ToxicDirection
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import spring.transaction.sample.ApplicationContainerInitializer
import spring.transaction.sample.FAILING_CONSUMER
import spring.transaction.sample.FAILING_KAFKA_PRODUCER
import spring.transaction.sample.INCOMING_TOPIC_KAFKA_FIRST
import spring.transaction.sample.MESSAGES
import spring.transaction.sample.OUTGOING_TOPIC_DB_FIRST
import spring.transaction.sample.OUTGOING_TOPIC_KAFKA_FIRST

@Component
class NestedKafkaTransactionCommittedFirst(val kafkaTemplate: KafkaTemplate<String, String>, val jdbcTemplate: JdbcTemplate) {

    @Value("\${outgoing.topic.kafka.first:$OUTGOING_TOPIC_KAFKA_FIRST}")
    lateinit var topicOutgoingKafkaFirst: String

    @KafkaListener(id = "group3", topics = ["\${incoming.topic.kafka.first:$INCOMING_TOPIC_KAFKA_FIRST}"])
    @Transactional("dstm")
    fun listen1(record: ConsumerRecord<String, String>) {
        println("Consuming message ${record.value()} - topic ${record.topic()} - offset ${record.offset()}")

//        if (System.getProperty(FAILING_KAFKA_PRODUCER, "false").toBoolean()) {
//            val toxics = ApplicationContainerInitializer.proxy.toxics()
//            if (toxics.all.isEmpty()) {
//                toxics.bandwidth("CUT_CONNECTION_DOWNSTREAM", ToxicDirection.DOWNSTREAM, 0)
//                toxics.bandwidth("CUT_CONNECTION_UPSTREAM", ToxicDirection.UPSTREAM, 0)
//            }
//        }

        insertToDatabase(record)
    }

    @Transactional("dstm")
    fun insertToDatabase(record: ConsumerRecord<String, String>) {
        println("Inserting into DB")
        jdbcTemplate.execute("insert into mytable (data) values ('${record.value()}')")
        sendToKafka(record)

        if (System.getProperty(FAILING_CONSUMER, "false").toBoolean()) {
            throw RuntimeException("failing consumer")
        }
    }

    @Transactional("kafkaTransactionManager")
    fun sendToKafka(record: ConsumerRecord<String, String>) {
        println("Sending to KAFKA")
        var send = kafkaTemplate.send(topicOutgoingKafkaFirst, record.value())
        send.whenComplete { meta, exception ->
            if (exception != null) {
                println("UNABLE TO SEND message ${record.value()}")
            }
        }
    }


    @KafkaListener(id = "group4", topics = ["\${outgoing.topic.kafka.first:$OUTGOING_TOPIC_KAFKA_FIRST}"])
    fun listen2(message: String) {
        println("Receiving Message $message")
        MESSAGES.add(message)
    }
}